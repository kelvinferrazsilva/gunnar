#!/bin/bash

#
# Developed and maintaince by 4bcloud.io
# 

# Update sistem
yum update -y

# Install utils application
yum install htop vim net-tools docker -y

# Enable Docker Service
service start docker && service enable docker

# Add user ec2-user to Docker Group
usermod -aG docker ec2-user
